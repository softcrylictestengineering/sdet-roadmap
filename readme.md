![Software Developement Engineer In Test](https://bitbucket.org/softcrylictestengineering/sdet-roadmap/raw/5e6526a130ee41b2a0674682ced98a7039f311e8/images/title.png)

> Roadmap to becoming a Software Developement Engineer In Test in 2019

Below you find a set of charts demonstrating the paths that you can take and the technologies that you would want to adopt in order to become a SDET. I made these charts for experienced and freshers who are working in test engineering to give them a perspective.

## Disclaimer
> The purpose of this roadmap is to give you an idea about the landscape and to guide you if you are confused about what to learn next and not to encourage you to pick what is hip and trendy. You should grow some understanding of why one tool would better suited for some cases than the other and remember hip and trendy never means best suited for the job

## 🚀 Introduction

![Software Development Engineer In Test Introduction](https://bitbucket.org/softcrylictestengineering/sdet-roadmap/raw/5e6526a130ee41b2a0674682ced98a7039f311e8/images/intro.png)

## 🎨 Functional Test Automation Roadmap

![Functional Test Automation Roadmap](https://bitbucket.org/softcrylictestengineering/sdet-roadmap/raw/5e6526a130ee41b2a0674682ced98a7039f311e8/images/Functional-test-automation.png)

## 👽 Non-functional Test Automation Roadmap

![Non-functional Test Automation Roadmap](https://bitbucket.org/softcrylictestengineering/sdet-roadmap/raw/5e6526a130ee41b2a0674682ced98a7039f311e8/images/Non-functional-test-automation.png)

## 👷 Exploratory Testing Roadmap

![Exploratory Testing Roadmap](https://bitbucket.org/softcrylictestengineering/sdet-roadmap/raw/4576368d0cbaeabaa9d787cc7b534f51fcae6310/images/Explorartory-Testing.png)

## 🚦 Wrap Up

If you think any of the roadmaps can be improved, please do open a PR with any updates and submit any issues. Also, I will continue to improve this, so you might want to watch/star this repository to revisit.

## 🙌 Contribution

> Have a look at [contribution docs](./contributing.md) for how to update any of the roadmaps

- Open pull request with improvements
- Discuss ideas in issues
- Spread the word
- Reach out with any feedback  [![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/sundarsritharan.svg?style=social&label=Follow%20%40sundarsritharan)](https://twitter.com/sundarsritharan)

## Sponsored By

- [Softcrylic - Digital Transformation through IT solutions and services](https://www.softcrylic.com)

## License

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)


